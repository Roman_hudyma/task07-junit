package com.epam;

import com.epam.MyTest.MinesweeperTest;
import com.epam.MyTest.PlateauTest;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

@SelectClasses({MinesweeperTest.class,PlateauTest.class})
@RunWith(JUnitPlatform.class)
public class TestSuite {
}
