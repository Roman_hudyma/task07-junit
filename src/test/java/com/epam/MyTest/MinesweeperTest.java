package com.epam.MyTest;

import com.epam.minesweeper.controller.Minesweeper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

@ExtendWith(MockitoExtension.class)
public class MinesweeperTest {

	private static Class clazz;
	private static Minesweeper minesweeper;
	private static int bombAmount = 45;

	@Mock
	private Minesweeper mineController;

	@BeforeAll
	public static void init(){
		minesweeper = new Minesweeper(bombAmount);
		clazz = minesweeper.getClass();
	}

	@Test
	public void updateView(){
		Mockito.when(mineController.updateArea(0,0)).thenReturn(true);
		Assertions.assertTrue(mineController.updateArea(0,0));
		Mockito.verify(mineController).updateArea(0,0);
	}

	@Test
	public void constant() throws NoSuchFieldException, IllegalAccessException {
		final Field hide = clazz.getDeclaredField("hide");
		hide.setAccessible(true);
		Assertions.assertEquals("$", hide.get(minesweeper));
	}


}
