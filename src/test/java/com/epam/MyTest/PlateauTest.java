package com.epam.MyTest;

import com.epam.minesweeper.controller.PlateauController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.List;

public class PlateauTest {

	private static int [] intArray;
	private static int [] emptyArray;
	private static PlateauController plateau;

	@BeforeAll
	public static void makeArray(){
		plateau = new PlateauController();
		intArray = new int[]{1,2,2,5,5,5,3,4,1,2,3};
		emptyArray = new int[]{};
	}

	@Test
	public void all(){
		List<List<Integer>> all = plateau.all(intArray);
		Assertions.assertTrue(all.stream().count() == 4);
	}

	@Test
	public void allWithException(){
		Assertions.assertThrows(RuntimeException.class, () -> plateau.all(emptyArray) );
	}

	@Test
	@RepeatedTest(3)
	public void longest(){
		final List<Integer> longest = plateau.longest(intArray);
		Assertions.assertTrue( longest.size() == 3);
	}

}
