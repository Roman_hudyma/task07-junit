package com.epam.minesweeper.model;

import java.util.Scanner;

public class Plateau {
    private final Scanner scanner =new Scanner(System.in);

    private int[] number;
    public int[]setNumber()
    {
        System.out.println("Enter number of integers:");
        number=new int[scanner.nextInt()];
        System.out.println("Enter numbers:");
        for(int i=0;i<number.length;i++)
        {
            number[i]=scanner.nextInt();
        }
        return number;
    }

    public int[] getNumber() {
        return number;
    }
}
