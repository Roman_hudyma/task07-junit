package com.epam.minesweeper;

import com.epam.minesweeper.view.MineSweeperView;
import com.epam.minesweeper.view.PlateauView;

public class ApplicationRunner {
    public static void main(String[] args) {

        new MineSweeperView().run();
        new PlateauView().run();
    }
}
