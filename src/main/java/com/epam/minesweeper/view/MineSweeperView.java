package com.epam.minesweeper.view;

import com.epam.minesweeper.controller.MineController;
import com.epam.minesweeper.controller.Minesweeper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MineSweeperView {

	private static final Logger logger = LogManager.getLogger();

	public void run() {
		System.out.println("Enter amount bomb: ");
		int amountBomb = scanner();
		boolean gameOver = false;
		MineController mineController = new Minesweeper(amountBomb);
		while (!gameOver) {
			System.out.println("Enter row");
			int row = scanner();
			System.out.println("Enter column");
			int column = scanner();
			gameOver = mineController.updateArea(row, column);
		}
		System.out.println("Game over");
	}

	private static int scanner(){
		int value = 0;
		try {
			Scanner scanner = new Scanner(System.in);
			value = scanner.nextInt();
		}catch (InputMismatchException e){
			logger.error(e);
		}
		return value;
	}

}
