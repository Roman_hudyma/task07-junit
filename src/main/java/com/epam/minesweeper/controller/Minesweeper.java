package com.epam.minesweeper.controller;

import java.util.Random;

public class Minesweeper implements MineController {

	private  final int areaHorizontal = 15;
	private final int areaVertical = 10;
	private final int border = 2;
	private final int index = 1;
	private final int empty = 0;
	private final String hide = "$";
	protected static final String BOMB = "*";
	private final String notBomb = ".";
	private final boolean[][] bombs = new boolean[areaVertical][areaHorizontal];
	private final String[][] area = new String[areaVertical][areaHorizontal];
	private final int[][] amountBombs = new int[areaVertical][areaHorizontal];

	public Minesweeper(int amountBomb) {
		generateBomb(amountBomb);
		generateSumBomb(bombs);
		defaultArea();
		printArea();
	}

	@Override
	public boolean updateArea(int row, int column) {
		boolean live = changeArea(row, column);
		printArea();
		return live;
	}

	private void generateBomb(int amount) {
		for (int i = 0; i < amount; i++) {
			int horizontal = new Random().nextInt(areaHorizontal - border) + index;
			int vertical = new Random().nextInt(areaVertical - border) + index;
			bombs[vertical][horizontal] = true;
		}
	}

	private void defaultArea() {
		for (int i = 0; i < bombs.length; i++) {
			for (int j = 0; j < bombs[i].length; j++) {
				area[i][j] = hide;
			}
		}
	}

	private void printArea() {
		System.out.println("column 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 10");
		for (int i = 0; i < area.length; i++) {
			System.out.print("row:   " + i + " ");
			for (int j = 0; j < area[i].length; j++) {
				System.out.print(area[i][j] + " ");
			}
			System.out.println();
		}
	}

	private void generateSumBomb(boolean[][] bomb) {
		for (int i = 0; i < bomb.length; i++) {
			for (int j = 0; j < bomb[i].length; j++) {
				if (bomb[i][j]) {
					amountBombs[i - index][j] += index;
					amountBombs[i + index][j] += index;
					amountBombs[i][j - index] += index;
					amountBombs[i][j + index] += index;
					amountBombs[i - index][j - index] += index;
					amountBombs[i + index][j + index] += index;
					amountBombs[i - index][j + index] += index;
					amountBombs[i + index][j - index] += index;
				}
			}
		}
	}

	private boolean changeArea(int row, int column) {
		if (bombs[row][column]) {
			area[row][column] = BOMB;
			return true;
		} else {
			openArea(row , column);
		}
		return false;
	}


	private void openArea(int row, int column) {
		if (area[row][column].equals(hide)) {
			area[row][column] = bombs[row][column]
					? hide : amountBombs[row][column] == empty
					? notBomb : String.valueOf(amountBombs[row][column]);
			if ((row > empty && row <= areaVertical - border)
					&& (column > empty && column <= areaHorizontal - border)) {
				area[row - index][column] = bombs[row - index][column]
						? hide : amountBombs[row - index][column] == empty
						? notBomb : String.valueOf(amountBombs[row - index][column]);
				area[row + index][column] = bombs[row + index][column]
						? hide : amountBombs[row + index][column] == empty
						? notBomb : String.valueOf(amountBombs[row + index][column]);
				area[row][column - index] = bombs[row][column - index]
						? hide : amountBombs[row][column - index] == empty
						? notBomb : String.valueOf(amountBombs[row][column - index]);
				area[row][column + index] = bombs[row][column + index]
						? hide : amountBombs[row][column + index] == empty
						? notBomb : String.valueOf(amountBombs[row][column + index]);
				area[row - index][column - index] = bombs[row - index][column - index]
						? hide : amountBombs[row - index][column - index] == empty
						? notBomb : String.valueOf(amountBombs[row - index][column - index]);
				area[row + index][column + index] = bombs[row + index][column + index]
						? hide : amountBombs[row + index][column + index] == empty
						? notBomb : String.valueOf(amountBombs[row + index][column + index]);
				area[row - index][column + index] = bombs[row - index][column + index]
						? hide : amountBombs[row - index][column + index] == empty
						? notBomb : String.valueOf(amountBombs[row - index][column + index]);
				area[row + index][column - index] = bombs[row + index][column - index]
						? hide : amountBombs[row + index][column - index] == empty
						? notBomb : String.valueOf(amountBombs[row + index][column - index]);
			}
		}
	}

}
