package com.epam.minesweeper.controller;

public interface MineController {

	boolean updateArea(int row, int column);

}
