package com.epam.minesweeper.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PlateauController {

	public List<Integer> longest(int[] data) {
		return all(data).stream()
				.max(Comparator.comparingInt(List::size))
				.orElse(new ArrayList<>());
	}

	public List<List<Integer>> all(int[] data) {
		if (data.length == 0) {
			throw new IllegalArgumentException();
		}
		List<List<Integer>> listPlateau = new ArrayList<>();
		List<Integer> plateau = new ArrayList<>();
		int current = data[0];
		for (int i = 0; i < data.length; i++) {
			if (current == data[i]) {
				plateau.add(i);

			} else {
				current = data[i];
				listPlateau.add(plateau);
				plateau = new ArrayList<>();
				plateau.add(i);
			}
		}
		listPlateau.add(plateau);
		return listPlateau;
	}

}
